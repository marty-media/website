# Configuration

## Overview
All configurations can be set using environment variables or by providing a configuration file. If the same option is provided twice, the following priority is used:
- Environment Variable
- Configuration File
- Default Value

## Configuration File
You can create a configuration file (json) to provide some options.

::: code-group

```json [config.json]
{
    "webServer": {
        "listen": "localhost:80"
    },
    "database": {
        "type": "postgres"
    }
}
```

:::

Then provide the file name and path as an argument during Marty Media Server startup:
```
./marty-media-server server -config=config.json
```

## Options
### Database
| Environment Variable                         | Configuration File Path                | Default Value |
|----------------------------------------------|----------------------------------------|---------------|
| `MMS_DATABASE_TYPE`                          | `database.type`                        | `"postgres"`  |
| `MMS_DATABASE_POSTGRES_ADDRESS`              | `database.postgres.address`            | `""`          |
| `MMS_DATABASE_POSTGRES_PORT`                 | `database.postgres.port`               | `5432`        |
| `MMS_DATABASE_POSTGRES_USER`                 | `database.postgres.user`               | `""`          |
| `MMS_DATABASE_POSTGRES_PASSWORD`             | `database.postgres.password`           | `""`          |
| `MMS_DATABASE_POSTGRES_DATABASE`             | `database.postgres.database`           | `""`          |
| `MMS_DATABASE_POSTGRES_MAX_OPEN_CONNECTIONS` | `database.postgres.maxOpenConnections` | `8`           |

### Storage
| Environment Variable    | Configuration File Path | Default Value                                                 |
|-------------------------|-------------------------|---------------------------------------------------------------|
| `MMS_STORAGE_TEMP_DIR`  | `storage.tempDir`       | default temporary directory depending on the operating system |
| `MMS_STORAGE_CACHE_SIZE`| `storage.cache.size`    | `200000000` (200 GB)                                           |

### WebServer
By default Marty Media Server starts on port `43200`. So you can access the server using the following URL:
[http://localhost:43200](http://localhost:43200)

The following additional URLs are provided by the server:
- `/web` serves the UI / main frontend
- `/api/v1` serves the API endpoint for all server actions
- `/apidoc/v1/swagger/index.html` serves the documentation for the API endpoint

| Environment Variable  | Configuration File Path | Default Value |
|-----------------------|-------------------------|---------------|
| `MMS_WEBSERVER_LISTEN` | `webServer.listen`     | `":43200"`    |

### FFmpeg
Most installation delivers its own FFmpeg version. Depending on the installation type a different default value might be automatically set (e.g. using Docker or Debian).

| Environment Variable  | Configuration File Path | Default Value |
|-----------------------|-------------------------|---------------|
| `MMS_FFMPEG_LOCATION` | `ffmpeg.location`       | `""`          |

## Using HTTPS
We recommend using the NGINX webserver as a proxy infront of the Marty Media Server.

::: warning
This chapter is work in progress.
:::
