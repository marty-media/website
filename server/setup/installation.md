<script setup>
  import { ref } from 'vue';
  import { serverVersion as sv, serverImage as si, serverBetaVersion as sbv, serverBetaImage as sbi } from '../container.js';

  const serverVersion = ref(sv);
  const serverImage = ref(si);
  const serverBetaVersion = ref(sbv);
  const serverBetaImage = ref(sbi);
</script>

# Installation

Please choose your prefered installation method based on your environment.

## Docker

### Docker Image

Use the following docker image to run the latest version of the Marty Media Server:
```-vue
{{ serverImage }}:latest
```

You can also use the version number as a tag for a fixed release:
```-vue
{{ serverImage }}:{{ serverVersion }}
```

Beta versions are available using:
```-vue
{{ serverBetaImage }}:latest
```

or using a fixed version for a beta release:
```-vue
{{ serverBetaImage }}:{{ serverBetaVersion }}
```

### Run Docker Image

The following command can be used as a template to run Marty Media Server. Don't forget the change the environment variables and mount folders:

```shellscript-vue
docker run -d \
    -p 43200:43200 \
    --env MMS_UID=1001 --env MMS_GID=1001 \
    --env MMS_DATABASE_POSTGRES_ADDRESS=postgresdb \
    --env MMS_DATABASE_POSTGRES_USER=martymedia \
    --env MMS_DATABASE_POSTGRES_PASSWORD=martymedia \
    --env MMS_DATABASE_POSTGRES_DATABASE=martymedia \
    --mount type=bind,source="/my/folder/config",destination="/app/config" \
    --mount type=bind,source="/my/folder/media",destination="/app/media" \
    --name marty-media-server \
    {{ serverImage }}:{{ serverVersion }}
```

## Docker Compose

The following sample can be used as a template to run Marty Media Server including PostgreSQL:

::: code-group

```yaml-vue [docker-compose.yml]
version: '3.5'
services:
  postgresdb:
    image: postgres:15
    container_name: martymedia-postgres
    user: 1001:1001
    environment:
      - POSTGRES_USER=martymedia
      - POSTGRES_PASSWORD=martymedia
      - POSTGRES_DB=martymedia
    volumes:
      - /my/folder/database/data:/var/lib/postgresql/data
    restart: 'unless-stopped'
  martymedia:
    image: {{ serverImage }}:{{ serverVersion }}
    container_name: martymedia-server
    environment:
      - MMS_UID=1001
      - MMS_GID=1001
      - MMS_DATABASE_POSTGRES_ADDRESS=postgresdb
      - MMS_DATABASE_POSTGRES_USER=martymedia
      - MMS_DATABASE_POSTGRES_PASSWORD=martymedia
      - MMS_DATABASE_POSTGRES_DATABASE=martymedia
    volumes:
      - /my/folder/media:/media
    restart: 'unless-stopped'
    ports:
      - "43200:43200"
```

:::

The following configuration options should be adjusted:
- user ID `1001` to the local user ID
- `POSTGRES_PASSWORD` and `MMS_DATABASE_POSTGRES_PASSWORD` to a new strong password
- `/my/folder/database/data` to the local folder for database files
- `/my/folder/media` to your local media folder

See chapter [Docker Image](#docker-image) for additional tags of the `image`.

## Kubernetes

### K8s

The following sample can be used as a template to run Marty Media Server including PostgreSQL:

::: code-group

```yaml-vue [database.yaml]
apiVersion: v1
kind: Secret
metadata:
  name: marty-media-server-db
type: kubernetes.io/basic-auth
stringData:
  username: martymedia
  password: martymedia
  db: martymedia
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: postgres
spec:
  selector:
    matchLabels:
      app: postgres
  template:
    metadata:
      labels:
        app: postgres
    spec:
      containers:
        - name: postgres
          image: postgres:15
          ports:
            - name: db
              containerPort: 5432
              protocol: TCP
          env:
            - name: POSTGRES_USER
              valueFrom:
                secretKeyRef:
                  name: marty-media-server-db
                  key: username
            - name: POSTGRES_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: marty-media-server-db
                  key: password
            - name: POSTGRES_DB
              valueFrom:
                secretKeyRef:
                  name: marty-media-server-db
                  key: db
---
kind: Service
apiVersion: v1
metadata:
  name: postgres
spec:
  type: ClusterIP
  selector:
    app: postgres
  ports:
    - protocol: TCP
      port: 5432
      targetPort: db
```

```yaml-vue [deployment.yaml]
apiVersion: apps/v1
kind: Deployment
metadata:
  name: marty-media-server-deployment
  labels:
    app: marty-media-server
spec:
  replicas: 3
  selector:
    matchLabels:
      app: marty-media-server
  template:
    metadata:
      labels:
        app: marty-media-server
    spec:
      containers:
        - name: marty-media-server
          image: {{ serverImage }}:{{ serverVersion }}
          ports:
            - name: http
              containerPort: 43200
              protocol: TCP
          env:
            - name: MMS_DATABASE_POSTGRES_ADDRESS
              value: postgres
            - name: MMS_DATABASE_POSTGRES_USER
              valueFrom:
                secretKeyRef:
                  name: marty-media-server-db
                  key: username
            - name: MMS_DATABASE_POSTGRES_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: marty-media-server-db
                  key: password
            - name: MMS_DATABASE_POSTGRES_DATABASE
              valueFrom:
                secretKeyRef:
                  name: marty-media-server-db
                  key: db
---
kind: Service
apiVersion: v1
metadata:
  name: marty-media-server
spec:
  type: ClusterIP
  selector:
    app: marty-media-server
  ports:
    - protocol: TCP
      port: 43200
      targetPort: http
```

:::

### Helm

Add the repository using:

```
helm repo add marty-media https://helm.marty-media.org
```

Install App using: 
```
helm upgrade --install marty-media-server marty-media/marty-media-server --create-namespace --namespace marty-media
```

Check out the full documentation on [ArtifactHub](https://artifacthub.io/packages/helm/martymedia/marty-media-server).
