export const serverVersion = "0.6.0";
export const serverImage = "registry.marty-media.org/server/release";
export const serverBetaVersion = "0.6.0-beta.1";
export const serverBetaImage = "registry.marty-media.org/server/beta";
