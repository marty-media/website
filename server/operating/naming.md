# Naming Convention

## Movies
The following rules for the folder structure are required:
- each movie has a separate folder
- the name of the folder contains at least the name of the movie (e.g. `The Matrix`)
    - optionally add the release year in braces (e.g. `The Matrix (1999)`)
- every file within the a movie folder is related to the movie (e.g. one file for the video, multiple subtitle files)
- a file can also contain multiple streams (multiple audio streams e.g. for multiple languages)

Example Structure:
```
+ The Matrix (1999)
    + The Matrix (1999).mp4       --> The Main File (contains video, audio, subtitles)
    + The Matrix (1999).de.srt    --> Subtitle File (German)
    + The Matrix (1999).en.srt    --> Subtitle File (English)
+ The Wolf of Wall Street (2013)
    + ...
```
