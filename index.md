---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "Marty Media"
  text: "Your own Streaming Service"
  tagline: Expect more soon...
  image:
    src: /logo.png
    alt: Marty Media
#  actions:
#    - theme: brand
#      text: Markdown Examples
#      link: /markdown-examples
#    - theme: alt
#      text: API Examples
#      link: /api-examples

#features:
#  - title: Feature A
#    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
#  - title: Feature B
#    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
#  - title: Feature C
#    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
---

